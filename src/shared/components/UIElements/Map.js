import React, { useRef, useEffect } from 'react';

import './Map.css';

const Map = props => {
  console.log(props.center);
  const mapRef = useRef();

  const { center, zoom } = props;

  // Runs when the components mounts, the map creation will run
  // after the jsx code has been rendered.
  useEffect(() => {
    const map = new window.google.maps.Map(mapRef.current, {
      center: center,
      zoom: zoom
    });

    new window.google.maps.Marker({ position: center, map: map });
  }, [center, zoom]);

  return (
    <div
      ref={mapRef}
      className={`map ${props.className}`}
      style={props.style}
    ></div>
  );
};

export default Map;
