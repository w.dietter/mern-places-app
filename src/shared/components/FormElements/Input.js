import React, { useReducer, useEffect } from 'react';
/* validate is called inside of input, but validators are provided from the parent component. */
import { validate } from 'shared/util/validators';
import './Input.css';

//reducer function for input state management
const inputReducer = (state, action) => {
  switch (action.type) {
    case 'CHANGE':
      return {
        ...state,
        value: action.value,
        isValid: validate(action.value, action.validators)
      };
    case 'TOUCH':
      return {
        ...state,
        isTouched: true
      };
    default:
      return state;
  }
};

const Input = props => {
  /* The component state manages the user input and the validation of the input. Validation criteria is provided from the parent component through the validators array prop. value and isValid are interconnected. Validity depends on the input value. */
  const [inputState, dispatch] = useReducer(inputReducer, {
    value: props.initialValue || '',
    isValid: props.initialValid || false,
    isTouched: false
  });
  const { id, onInput } = props;
  const { value, isValid } = inputState;

  useEffect(() => {
    onInput(id, value, isValid);
  }, [id, value, isValid, onInput]);

  /* triggered on every key stroke that the user enters */
  const changeHandler = event => {
    dispatch({
      type: 'CHANGE',
      value: event.target.value,
      validators: props.validators
    });
  };

  const touchHandler = () => {
    dispatch({
      type: 'TOUCH'
    });
  };

  const element =
    /* element: 'input' : 'textarea' */
    props.element === 'input' ? (
      <input
        onChange={changeHandler}
        /* triggers a state change -> isTouched value to true */
        onBlur={touchHandler}
        id={props.id}
        type={props.type}
        placeholder={props.placeholder}
        value={inputState.value}
      />
    ) : (
      <textarea
        onChange={changeHandler}
        /* triggers a state change -> isTouched value to true */
        onBlur={touchHandler}
        id={props.id}
        rows={props.rows || 3}
        value={inputState.value}
      />
    );

  return (
    <div
      className={`form-control ${!inputState.isValid &&
        inputState.isTouched &&
        'form-control--invalid'} `}
    >
      <label htmlFor={props.id}>{props.label}</label>
      {/* element: input || textarea */}
      {element}
      {!inputState.isValid && inputState.isTouched && <p>{props.errorText}</p>}
    </div>
  );
};

export default Input;
