import React, { useContext } from 'react';
import { AuthContext } from 'shared/context/auth-context';
import { NavLink } from 'react-router-dom';
import Button from 'shared/components/FormElements/Button';

import './NavLinks.css';
const NavLinks = props => {
  const auth = useContext(AuthContext);
  console.log(auth);
  return (
    <ul className="nav-links">
      <li>
        <NavLink to="/" exact>
          All Users
        </NavLink>
      </li>
      {auth.isLoggedIn && (
        <li>
          <NavLink to={auth.userId ? auth.userId : ''}>My Places</NavLink>
        </li>
      )}
      {auth.isLoggedIn && (
        <li>
          <NavLink to="/places/new">Add Place</NavLink>
        </li>
      )}
      {!auth.isLoggedIn && (
        <li>
          <NavLink to="/auth">Authenticate</NavLink>
        </li>
      )}
      {auth.isLoggedIn && (
        <li>
          <Button onClick={auth.logout} danger>
            Logout
          </Button>
        </li>
      )}
    </ul>
  );
};

export default NavLinks;
