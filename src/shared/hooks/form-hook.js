import { useCallback, useReducer } from 'react';

/* manages the validity of the whole form */
const formReducer = (state, action) => {
  switch (action.type) {
    case 'INPUT_CHANGE':
      let formIsValid = true;
      for (const inputId in state.inputs) {
        if (!state.inputs[inputId]) {
          /* skip the loop */
          continue;
        }
        if (inputId === action.payload.inputId) {
          formIsValid = formIsValid && action.payload.isValid;
        } else {
          formIsValid = formIsValid && state.inputs[inputId].isValid;
        }
      }
      return {
        ...state,
        inputs: {
          ...state.inputs,
          [action.payload.inputId]: {
            value: action.payload.value,
            isValid: action.payload.isValid
          }
        },
        isValid: formIsValid
      };
    case 'SET_DATA':
      return {
        inputs: action.payload.inputs,
        isValid: action.payload.formIsValid
      };
    default:
      return state;
  }
};

export const useForm = (initialInputs, initialFormValidity) => {
  const [formState, dispatch] = useReducer(formReducer, {
    // individual inputs state is controlled here.
    inputs: initialInputs,
    //Overall form validity.
    isValid: initialFormValidity
  });

  /* useCallback returns a memoized version of the function that only changes when one of inputs change. Wraps a function and define dependencies on which the function will be recreated. If no dependencies are provided the function is memoized. */
  const inputHandler = useCallback((id, value, isValid) => {
    /* manage form validity */
    /* this function is passed to the INPUT component. */
    dispatch({
      type: 'INPUT_CHANGE',
      payload: {
        /* points to the specific input */
        inputId: id,
        /* value of the input component */
        value,
        /* validity of the specific input */
        isValid
      }
    });
  }, []);

  const setFormData = useCallback((inputData, formValidity) => {
    dispatch({
      type: 'SET_DATA',
      payload: {
        inputs: inputData,
        formIsValid: formValidity
      }
    });
  }, []);

  return [formState, inputHandler, setFormData];
};
